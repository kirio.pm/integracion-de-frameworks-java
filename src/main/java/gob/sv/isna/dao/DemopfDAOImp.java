package gob.sv.isna.dao;

import gob.sv.isna.dao.DemopfDAO;
import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import gob.sv.isna.modelo.Rol;
import gob.sv.isna.modelo.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

 @Repository
public class DemopfDAOImp implements DemopfDAO, Serializable {

	private static final long serialVersionUID = 1L;
        
        @Autowired
	private SessionFactory sessionFactory;

	// Usuario

	@Override
	public Usuario obtenerUsuarioXUsuario(String usuario) {
		Session session = null;

		try {
			session = sessionFactory.openSession();
			return session.createQuery("FROM Usuario us JOIN FETCH us.rol WHERE us.usuario = :usuario",Usuario.class).setParameter("usuario", usuario).getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
				session = null;
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Usuario> obtenerUsuarios() {

		Session session = null;

		try {
			session = sessionFactory.openSession();
			return session.createQuery("FROM Usuario us JOIN FETCH us.rol",Usuario.class).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
				session = null;
			}
		}

	}

	@Override
	public void guardarUsuario(Usuario usuario) {

		Session session = null;
		Transaction tx = null;

		try {

			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.persist(usuario);
			tx.commit();

		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
				session = null;
			}
		}

	}

	@Override
	public void actualizarUsuario(Usuario usuario) {

		Session session = null;
		Transaction tx = null;

		try {

			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.update(usuario);
			tx.commit();

		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
				session = null;
			}
		}

	}

	@Override
	public void eliminarUsuario(Usuario usuario) {

		Session session = null;
		Transaction tx = null;

		try {

			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			Usuario u = session.load(Usuario.class, usuario.getId());
			session.delete(u);
			tx.commit();

		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
				session = null;
			}
		}

	}

	@Override
	public Usuario login(Usuario usuario) {
		Session session = null;

		try {
			session = sessionFactory.openSession();
			return session.createQuery("FROM Usuario us JOIN FETCH us.rol rl WHERE us.usuario = :usuario AND us.contrasenia = :contrasenia", Usuario.class).setParameter("usuario", usuario.getUsuario()).setParameter("contrasenia", usuario.getContrasenia()).getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
				session = null;
			}
		}
	}

	// Rol

	
	@Override
	public List<Rol> obtenerRoles() {

		Session session = null;

		try {
			session = sessionFactory.openSession();
			return session.createQuery("FROM Rol", Rol.class).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
				session = null;
			}
		}
	}

	public Rol obtenerRolXId(int id) {
		Session session = null;

		try {
			session = sessionFactory.openSession();
			return session.createQuery("FROM Rol WHERE id = :id", Rol.class).setParameter("id", id).uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
				session = null;
			}
		}
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
