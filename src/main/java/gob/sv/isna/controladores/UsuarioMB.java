package gob.sv.isna.controladores;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import gob.sv.isna.modelo.Rol;
import gob.sv.isna.modelo.Usuario;
import gob.sv.isna.servicio.DemopfServicio;
import gob.sv.isna.util.MsgUtil;
import javax.inject.Named;
import org.springframework.beans.factory.annotation.Autowired;

@Named("usuarioMB")
@ViewScoped
public class UsuarioMB implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	DemopfServicio demopfServicio;

	private Usuario usuario;
	private Usuario slcUsuario;
	private boolean editar = false;
	private List<Usuario> filtroUsuarios;

	public UsuarioMB() {

	}

	@PostConstruct
	public void inicio() {

		usuario = new Usuario();

	}

	public void guardar() {

		if (!editar) {
			try {
				demopfServicio.guardarUsuario(usuario);
				this.cancelar();
				MsgUtil.msgInfo("Exito!", "Usuario guardado correctamente.");
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			try {
				demopfServicio.actualizarUsuario(usuario);
				this.cancelar();
				MsgUtil.msgWarning("Advertencia!", "Usuario actualizado.");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	public void actualizar() {
		usuario = slcUsuario;
		editar = true;
	}

	public void eliminar() {

		try {
			demopfServicio.eliminarUsuario(slcUsuario);
			MsgUtil.msgError("Error!", "Usuario eliminado correctamente.");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void cancelar() {
		usuario = new Usuario();
		editar = false;
	}

	public List<Usuario> getUsuarios() {
		return demopfServicio.obtenerUsuarios();
	}

	public List<Rol> getRoles() {
		return demopfServicio.obtenerRoles();
	}

	public void setDemopfServicio(DemopfServicio demopfServicio) {
		this.demopfServicio = demopfServicio;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Usuario getSlcUsuario() {
		return slcUsuario;
	}

	public void setSlcUsuario(Usuario slcUsuario) {
		this.slcUsuario = slcUsuario;
	}

	public List<Usuario> getFiltroUsuarios() {
		return filtroUsuarios;
	}

	public void setFiltroUsuarios(List<Usuario> filtroUsuarios) {
		this.filtroUsuarios = filtroUsuarios;
	}

}
