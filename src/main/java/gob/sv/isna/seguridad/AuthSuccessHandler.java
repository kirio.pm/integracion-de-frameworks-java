package gob.sv.isna.seguridad;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

public class AuthSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

	@Override
	protected String determineTargetUrl(HttpServletRequest request, HttpServletResponse response) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String role = auth.getAuthorities().toString();

		String targetUrl = null;

		if (role.contains("ADMINISTRADOR")) {
			targetUrl = "/vistas/administrador/adminUsuarios.xhtml";
		} else if (role.contains("USUARIO")) {
			targetUrl = "/vistas/usuario/usuarios.xhtml";
		}

		return targetUrl;
	}

}