package gob.sv.isna.servicio;

import java.io.Serializable;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import gob.sv.isna.dao.DemopfDAO;
import gob.sv.isna.modelo.Rol;
import gob.sv.isna.modelo.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
@Transactional(readOnly = true)
public class DemopfServicioImp implements DemopfServicio, Serializable {

	private static final long serialVersionUID = 1L;
	@Autowired
        private DemopfDAO demopfDAO;

	// Usuario

	@Override
	public List<Usuario> obtenerUsuarios() {
		return demopfDAO.obtenerUsuarios();
	}

	@Transactional(readOnly = false)
	@Override
	public void guardarUsuario(Usuario usuario) {
		demopfDAO.guardarUsuario(usuario);
	}

	@Transactional(readOnly = false)
	@Override
	public void actualizarUsuario(Usuario usuario) {
		demopfDAO.actualizarUsuario(usuario);
	}

	@Transactional(readOnly = false)
	@Override
	public void eliminarUsuario(Usuario usuario) {
		demopfDAO.eliminarUsuario(usuario);
	}

	@Override
	public Usuario login(Usuario usuario) {
		return demopfDAO.login(usuario);
	}

	// Rol

	@Override
	public List<Rol> obtenerRoles() {
		return demopfDAO.obtenerRoles();
	}

	public Rol obtenerRolXId(int id) {
		return demopfDAO.obtenerRolXId(id);
	}

	public void setDemopfDAO(DemopfDAO demopfDAO) {
		this.demopfDAO = demopfDAO;
	}

}
