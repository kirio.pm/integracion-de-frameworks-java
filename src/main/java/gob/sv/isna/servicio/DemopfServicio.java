package gob.sv.isna.servicio;

import java.util.List;

import gob.sv.isna.modelo.Rol;
import gob.sv.isna.modelo.Usuario;

public interface DemopfServicio {

	// Usuario

	public List<Usuario> obtenerUsuarios();

	public void guardarUsuario(Usuario usuario);

	public void actualizarUsuario(Usuario usuario);

	public void eliminarUsuario(Usuario usuario);

	public Usuario login(Usuario usuario);

	// Rol

	public List<Rol> obtenerRoles();

	public Rol obtenerRolXId(int id);

}
