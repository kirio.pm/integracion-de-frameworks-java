package gob.sv.isna.convertidores;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import gob.sv.isna.modelo.Rol;
import gob.sv.isna.servicio.DemopfServicio;
import javax.inject.Named;
import org.springframework.beans.factory.annotation.Autowired;

@Named("rolConverter")
@RequestScoped
public class RolConverter implements Converter {

	@Autowired
	DemopfServicio demopfServicio;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {

		try {

			if (value != null && value.trim().length() > 0) {
				return demopfServicio.obtenerRolXId(Integer.parseInt(value));
			} else {
				return null;
			}

		} catch (Exception e) {
			FacesMessage msg = new FacesMessage("Error!", "Rol no valido");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ConverterException(msg);
		}

	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {

		if (value != null) {
			return String.valueOf(((Rol) value).getId());
		} else {
			return null;
		}

	}

	public void setDemopfServicio(DemopfServicio demopfServicio) {
		this.demopfServicio = demopfServicio;
	}

}
